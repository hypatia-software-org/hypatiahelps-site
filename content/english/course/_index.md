---
title: "Hypatia Helps: Community Workshops"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Hypatia Helps is proud to offer a variety of trainings and workshops for our online community as well as other organizations, businesses, and local governments, we have seasoned presenters and customizable presentations that can help teach you the skills needed to effect the change you need with our educational workshops."
---
